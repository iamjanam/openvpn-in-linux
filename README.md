**OpenVPN Docker Setup Script**
This script sets up an OpenVPN Docker container, generates a CA password file, and creates a client configuration file.

**Prerequisites**
Docker installed on your system
`sudo apt install docker.io` 

**Steps**
- Open your terminal and navigate to the directory where you want to save the script.
- Create a new file with a .sh extension (e.g. openvpnsetup.sh) and paste the contents of the script into the file.
- Replace VPN.SERVERNAME.COM with your domain-name or ip of the server.
- Make the script executable by running chmod +x openvpnsetup.sh.
- Run the script with sudo privileges by executing sudo ./openvpnsetup.sh.
- The script will stop and remove any existing OpenVPN Docker containers, create a directory to store the OpenVPN data, delete any existing Docker volumes, create a new Docker volume and mount it to $HOME/ovpn-data on the host, start the OpenVPN Docker container, generate a CA password file, store the CA password key in $HOME/ovpn-data, create a client configuration file, and initialize the PKI and generate certificates and keys.
- Once the script is finished, you can connect to your VPN by downloading the client configuration file from $HOME/ovpn-data/users and importing it into your OpenVPN client.

**Creating Users with Passwords**

To create a new OpenVPN user with a password, use the following steps:

- Open your terminal and navigate to the directory where you saved the script.
- Run the script with sudo privileges by executing sudo ./crateusers.sh.
- When prompted, enter the new username for the user you want to create.
- The script will generate a random password for the user, create a new OpenVPN user with the given username and password, create a new directory for the user's files, and generate the client configuration for the user.
- The script will output the user's password.
- You can now connect to your VPN using the client configuration file for the user.
